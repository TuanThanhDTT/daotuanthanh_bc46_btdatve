import React from "react";
import data from "./data.json";
import ChairList from "./ChairList";
import Result from "./Result";
const HomePage = () => {
  return (
    <div className="HomePage">
      <div className="container">
        <h1 className="text-center p-5">MOVIE SEAT SELECTION</h1>
        <div className="row mt-5">
          <div className="col-8 magic">
            <h1>Khu vực chọn ghế</h1>
            <div className="text-center p-3 font-weight-bold screen text-white mt-3">
              SCREEN
            </div>
            {/* Danh sách ghế */}
            <ChairList data={data} />
          </div>
          <div className="col-4 result">
            {/* Kết quả đặt ghế */}
            <Result />
          </div>
        </div>
      </div>

      <br />
    </div>
  );
};

export default HomePage;
