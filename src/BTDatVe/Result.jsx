import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { chairBookingsAction, payAction } from "../store/BTDatVe/action";

const Result = () => {
  const { chairBookings, chairBooked } = useSelector((state) => state.btDatVe);
  const dispatch = useDispatch();
  return (
    <div>
      <h1>Thông tin đặt ghế</h1>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark Chair booked">
          Ghế đã đặt
        </button>
        <button className="btn btn-outline-dark Chair booking">
          Ghế đang chọn
        </button>
        <button className="btn btn-outline-dark">Ghế chưa đặt</button>
      </div>

      <table className="table">
        <thead>
          <tr>
            <td>Số ghế</td>
            <td>Giá</td>
            <td>Hủy</td>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe) => (
            <tr>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia}</td>
              <td>
                {""}
                <button
                  className="btn btn-danger"
                  onClick={() => {
                    dispatch(chairBookingsAction(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}

          {/* Tính tổng tiền */}
          <tr>
            <td>Tổng tiền</td>
            <td>
              {chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}
            </td>
          </tr>
          <tr>
            <button
              className="btn btn-success"
              onClick={() => {
                dispatch(payAction());
              }}
            >
              Thanh Toán
            </button>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Result;
