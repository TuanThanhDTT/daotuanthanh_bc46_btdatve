import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  return (
    <div>
      {data.map((hangGhe) => {
        return (
          <div className="d-flex mt-3 chairlist">
            <div className="hangGhe" style={{ width: "20px" }}>
              {hangGhe.hang}
            </div>
            <div className="d-flex" style={{ gap: "10px" }}>
              {hangGhe.danhSachGhe.map((ghe) => {
                return <Chair ghe={ghe} key={ghe.soGhe} />;
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ChairList;
