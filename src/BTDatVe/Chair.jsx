import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { chairBookingsAction } from "../store/BTDatVe/action";
import cn from "classnames";
import "./style.scss";
const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  const { chairBookings, chairBooked } = useSelector((state) => state.btDatVe);
  //   console.log("chairBookings",chairBookings);
  return (
    <div>
      <button
        className={cn("btn btn-outline-light Chair", {
          booking: chairBookings.find((e) => e.soGhe == ghe.soGhe),
          booked: chairBooked.find((e) => e.soGhe == ghe.soGhe),
        })}
        style={{ width: "50px" }}
        onClick={() => {
          dispatch(chairBookingsAction(ghe));
        }}
      >
        {ghe.soGhe}
      </button>
    </div>
  );
};

export default Chair;
