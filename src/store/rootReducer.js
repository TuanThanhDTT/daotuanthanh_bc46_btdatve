import { combineReducers } from "redux";
import { reducer } from "./BTDatVe/reducer";

export const rootReducer = combineReducers({
  btDatVe: reducer,
});
