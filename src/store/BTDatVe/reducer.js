import { HANDLE_CHAIR_BOOKINGS } from "./actionType";
import { PAY } from "./actionType";

const initialState = {
  chairBookings: [],
  chairBooked: [],
};

export const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case HANDLE_CHAIR_BOOKINGS: {
      const data = [...state.chairBookings];

      //Kiểm tra trong mảng chairBookings có tồn tại ghế mình dispatch(action) không?
      const index = data.findIndex((value) => value.soGhe == payload.soGhe);

      if (index == -1) {
        data.push(payload);
      } else {
        //Trường hợp user click dispatch(action) vị trí ghế cũ lần nữa => bỏ chọn
        data.splice(index, 1);
      }
      return { ...state, chairBookings: data };
    }

    case PAY: {
      const data = [...state.chairBooked, ...state.chairBookings];
      return { ...state, chairBooked: data, chairBookings: [] };
    }
    default:
      return state;
  }
};
